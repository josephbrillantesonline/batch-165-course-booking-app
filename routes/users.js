//[SECTION] Dependencies and Modules
  const exp = require("express"); 
  const controller = require('./../controllers/users.js');
  const auth = require("../auth") 
  const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
  const route = exp.Router(); 


//[SECTION] Routes-[POST]
  route.post('/register', (req, res) => {
    let userDetails = req.body; 
    controller.registerUser(userDetails).then(outcome => {
       res.send(outcome);
    });
  });
  


// LOGIN
  route.post("/login", controller.loginUser);



// GET USER DETAILS
  route.get("/getUserDetails", verify, controller.getUserDetails)




//ENROLL OUR REGISTERED USER
  route.post('/enroll', verify, controller.enroll)




//GET LOGGED USER'S ENROLLMENTS
route.get('/getEnrollments', verify, controller.getEnrollments)





//[SECTION] Expose Route System
  module.exports = route; 
