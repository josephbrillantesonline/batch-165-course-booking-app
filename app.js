// [SECTION] Packages and Dependencies
const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const courseRoutes = require('./routes/courses');

// [SECTION] Server Setup
	
const app = express();
dotenv.config(); 
app.use(express.json());
const secret = process.env.CONNECTION_STRING
const port = process.env.PORT

// [SECTION] Application Routes
	// lets implement the concept of separation of concerns within our routing components.
	// append the name of the collection within the designated routes
	app.use('/courses', courseRoutes);

// [SECTION] Database Connect
mongoose.connect(secret)
let connectStatus = mongoose.connection;
connectStatus.on('open', () => console.log('Database Connected'));


// [SECTION] Gateway REsponse
app.get('/', (req, res) => {
	res.send('Welcome to Zeke & Jappy');
});
app.listen(port, () => console.log(`Server is running on port ${port}`));


